import React from 'react';
import('./ThankYou.css');

export class ThankYou extends React.Component {

    render() {
        return (
            <div>
                <h4>Thank You !</h4>
                <div>You can find the source code by clicking this <a href="https://gitlab.com/Franofarion/demo-react-lunchr" rel="noopener noreferrer"
                        target="_blank" className="link-thx"> LINK </a> !
                </div>
            </div>
        );
    }
}
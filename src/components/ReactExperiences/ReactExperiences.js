import React from 'react';
import { Loading } from '../Loading/Loading';

import react from "./react.png";
import aroga from "./aroga.png";
import securify from "./securify.png";

import './ReactExperiences.css';

export class ReactExperiences extends React.Component {

    constructor() {
        super();
        this.state = { loading: true };
    }

    async componentDidMount() {
        let img1 = new Image();
        let img2 = new Image();
        let img3 = new Image();
        img1.src = react;
        img2.src = aroga;
        img3.src = securify;

        img1.onload = () => {
            img2.onload = () => {
                img3.onload = () => {
                    this.setState({ loading: false });
                }
            }
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div>
                    <Loading></Loading>
                </div>
            )
        } else {
            return (
                <div>
                    <h4>My React Experiences</h4>
                    <div className="experience">
                        <img className="img" src={react} alt="react"></img>
                        <div className="subTitle">React for Lunchr (this app)</div>
                        <div className="tech">React Router / Redux / Cordova</div>
                    </div>
                    <div className="experience">
                        <img className="img2" src={aroga} alt="aroga"></img>
                        <div className="subTitle">Klanik Akademy</div>
                        <div className="tech">React Native (TypeScript) / Expo / NestJs</div>
                    </div>
                    <div className="experience">
                        <img className="img2" src={securify} alt="securify"></img>
                        <div className="subTitle">Securify</div>
                        <div className="tech">React Native / React Navigation / Expo features </div>
                    </div>
                </div>
            );
        }
    }
}
import React from 'react';

// import './WhoAmI.css';

export class WhoAmI extends React.Component {

    render() {
        return(
            <div>
                <h4>Who am I ?</h4>
                <div className="description">
                    My name is Baptiste Domange and I'm a JavaScript developper highly interested by DevOps practices. 
                </div>
                <p></p>
                <div>
                    I just graduate from EPSI Montpellier and I'm joining Klanik this October.
                </div>
            </div>
        );
    }
}
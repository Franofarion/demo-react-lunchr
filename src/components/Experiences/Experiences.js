import React from 'react';
import { Loading } from '../Loading/Loading';

import pcp from "./pcp.jpg";
import appliphyto from "./appliphyto.png";
import ing from "./ing.png";

import './Experiences.css';

export class Experiences extends React.Component {

    constructor() {
        super();
        this.state = { loading: true };
    }

    async componentDidMount() {
        let img1 = new Image();
        let img2 = new Image();
        let img3 = new Image();
        img1.src = pcp;
        img2.src = appliphyto;
        img3.src = ing;

        img1.onload = () => {
            img2.onload = () => {
                img3.onload = () => {
                    this.setState({ loading: false });
                }
            }
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div>
                    <Loading></Loading>
                </div>
            )
        } else {
            return (
                <div>
                    <h4>Profesionnal Experiences</h4>
                    <div className="experience">
                        <img className="img" src={ing} alt="ing"></img>
                        <div>AngularJS / Java / Oracle</div>
                    </div>
                    <div className="experience">
                        <img className="img2" src={pcp} alt="pcp"></img>
                        <div>Angular / NodeJS / MongoDB</div>
                    </div>
                    <div className="experience">
                        <img className="img2" src={appliphyto} alt="appliphyto"></img>
                        <div>PHP / MySQL </div>
                    </div>
                </div>
            );
        }
    }
}
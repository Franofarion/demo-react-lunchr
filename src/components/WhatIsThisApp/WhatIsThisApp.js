import React from 'react';
import { Loading } from '../Loading/Loading';
import lunchr from "./lunchr.svg";

import './WhatIsThisApp.css';

export class WhatIsThisApp extends React.Component {

    constructor() {
        super();
        this.state = { loading: true };
    }

    componentDidMount() {
        let img = new Image();
        img.src = lunchr;

        img.onload = () => {
            this.setState({ loading: false });
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div>
                    <Loading></Loading>
                </div>
            )
        } else {
            return (
                <div>
                    <h4>The purpose of this application</h4>
                    <img src={lunchr} alt="logo lunchr"></img>
                    <div className="description">
                        This is a short presentation of my career and my skills for my Lunchr interview the 10/09/2019.
                    </div>
                </div>
            );
        }
    }
}
import React from 'react';
import('./Todo.css');

export class Todo extends React.Component {

    render() {
        return (
            <div>
                <h4>What I need to do now</h4>
                <ul className="list">
                    <li>Dig more redux</li>
                    <li>Have a look at React Hooks</li>
                </ul>
            </div>
        );
    }
}
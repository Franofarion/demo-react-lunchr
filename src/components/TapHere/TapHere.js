import React from 'react';
import tapGif from "./tap.gif";
import { Loading } from '../Loading/Loading';

import './TapHere.css'

export class TapHere extends React.Component {

    constructor() {
        super();
        this.state ={ loading: true };
    }

    componentDidMount() {
        let img = new Image();
        img.src = tapGif;

        img.onload = () => {
            this.setState({ loading: false });
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <div>
                    <Loading></Loading>
                </div>
            )
        } else {
            return (
                <div>
                    <div>You can tap on the screen to display the next page</div>
                    <img src={tapGif} alt="tap" className="gif"></img>
                </div>
            );
        }
    }
}
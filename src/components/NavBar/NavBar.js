import React from 'react';
import { Link } from "react-router-dom";
import returnImg from "./return.png";

import './NavBar.css';

export class NavBar extends React.Component {

    displayNavBar() {
        return this.props.location.pathname !== '/';
    }

    render() {
        if(this.displayNavBar()) {
            return (
                <div className="redirectHome">
                    <Link to={`/`}>
                        <img src={returnImg} className="svgImg" alt="return"/>
                    </Link>
                </div>
            );
        }
        return null;
    }
}
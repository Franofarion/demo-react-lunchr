import React from 'react';
import { connect } from 'react-redux';
import { NavBar } from '../../components/NavBar/NavBar';
import { TapHere } from '../../components/TapHere/TapHere';
import { WhatIsThisApp } from '../../components/WhatIsThisApp/WhatIsThisApp';
import { WhoAmI } from '../../components/WhoAmI/WhoAmI';
import { Experiences } from '../../components/Experiences/Experiences';
import { ReactExperiences } from '../../components/ReactExperiences/ReactExperiences';
import { Todo } from '../../components/Todo/Todo';

import './Demo.css';
import { ThankYou } from '../../components/ThankYou/ThankYou';

const PAGES = {
    0: <TapHere />,
    1: <WhatIsThisApp />,
    2: <WhoAmI />,
    3: <Experiences />,
    4: <ReactExperiences />,
    5: <Todo />,
    6: <ThankYou />
}

class Demo extends React.Component {

    changeIndex(operator) {
        const { index, min, max } = this.props;
        switch (operator) {
            case '+':
                if (index < max)
                    this.props.dispatch({ type: 'NEXT_PAGE' });
                break;
            case '-':
                if (index > min)
                    this.props.dispatch({ type: 'PREVIOUS_PAGE' });
                break;
            default:
        }
    }

    displayBackButton() {
        return this.props.index > this.props.min;
    }

    render() {
        return (
            <div>
                <NavBar {...this.props} />
                <div className="demo">
                    <div className="main" onClick={() => this.changeIndex('+')}>
                        <div>
                            {PAGES[this.props.index]}
                        </div>
                    </div>
                    {
                        this.displayBackButton() &&
                        <div className="back" onClick={() => this.changeIndex('-')}> Back </div>
                    }
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { pages } = state;
    return pages;
}

export default connect(mapStateToProps)(Demo);

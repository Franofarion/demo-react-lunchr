import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { NavBar } from '../../components/NavBar/NavBar';

import './Home.css';

class Home extends React.Component {

    render() {
        return (
            <div>
                <NavBar {...this.props} />
                <div className="menu">
                    <Link to={`/demo`} className="link">
                        {this.props.index === this.props.min && <div> Begin </div>}
                        {(this.props.index > this.props.min && this.props.index < this.props.max) &&
                            <div>
                                <div>
                                    Continue
                                </div>
                                <div>
                                    {this.props.index + 1} / {this.props.max + 1}
                                </div>
                            </div>
                        }
                        {this.props.index === this.props.max && <div> Done </div>}
                    </Link>
                    <Link to={`/about`} className="link">
                        <div> About me</div>
                    </Link>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { pages } = state;
    return pages;
}

export default connect(mapStateToProps)(Home);
import React from 'react';
import { NavBar } from '../../components/NavBar/NavBar';
import { Loading } from '../../components/Loading/Loading';
import gitlab from "./gitlab.png";
import linkedin from "./linkedin.png";
import www from "./www.png";

import('./About.css');


export class About extends React.Component {

    constructor() {
        super();
        this.state = { loading: true };
    }

    async componentDidMount() {
        let img1 = new Image();
        let img2 = new Image();
        let img3 = new Image();
        img1.src = gitlab;
        img2.src = linkedin;
        img3.src = www;

        img1.onload = () => {
            img2.onload = () => {
                img3.onload = () => {
                    this.setState({ loading: false });
                }
            }
        }
    }

    render() {
        return (
            <div>
                <NavBar {...this.props} />
                {this.state.loading && <div> <Loading></Loading></div>}
                {
                    !this.state.loading &&
                    <div>
                        <h3>Find all informations on social media</h3>
                        <a href="https://gitlab.com/Franofarion/" rel="noopener noreferrer" target="_blank">
                            <img className="social" src={gitlab} alt="gitlab"></img>
                        </a>
                        <a href="https://www.linkedin.com/in/baptistedomange/" rel="noopener noreferrer" target="_blank">
                            <img className="social" src={linkedin} alt="linkedin"></img>
                        </a>
                        <a href="https://baptiste-domange.fr" rel="noopener noreferrer" target="_blank">
                            <img className="social" src={www} alt="www"></img>
                        </a>
                    </div>
                }
            </div>

        );
    }
}

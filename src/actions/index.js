import * as types from '../constants/ActionTypes'

export const nextPage = number => ({ type: types.NEXT_PAGE, number })
export const previousPage = number => ({ type: types.PREVIOUS_PAGE, number })

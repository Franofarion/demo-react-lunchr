import * as types from '../constants/ActionTypes'
import * as actions from './index'

describe('todo actions', () => {
  it('nextPage should create NEXT_PAGE action', () => {
    expect(actions.addTodo(1)).toEqual({
      type: types.NEXT_PAGE,
      number: 1
    })
  })

  it('previousPage should create PREVIOUS_PAGE action', () => {
    expect(actions.addTodo(1)).toEqual({
      type: types.PREVIOUS_PAGE,
      number: 1
    })
  })
})
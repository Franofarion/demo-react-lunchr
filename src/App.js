import React from 'react';
import { HashRouter as Router, Route } from "react-router-dom";


import Home  from './containers/HomePage/Home'
import { About } from './containers/AboutPage/About';
import  Demo from './containers/DemoPage/Demo';

import './App.css';

export default class App extends React.Component {

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <Router>
            <Route exact path="/" component={Home} />
            <Route path="/demo" component={Demo} />
            <Route path="/about" component={About} />
          </Router>
        </div>
      </div>
    );
  }
}

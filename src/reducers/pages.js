import { NEXT_PAGE, PREVIOUS_PAGE } from '../constants/ActionTypes'

const initialState = {
    index: 0,
    min: 0,
    max: 6
}

export default function pages(state = initialState, action) {
    switch (action.type) {
        case NEXT_PAGE:
            return { index: state.index + 1, min: state.min, max: state.max }
        case PREVIOUS_PAGE:
            return { index: state.index - 1, min: state.min, max: state.max }
        default:
            return state;
    }
}